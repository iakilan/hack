package com.example.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TRIP")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Trip {
	//@JsonIgnore
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int tripId;

	@Column(name = "passenger_id")
	private int passengerId;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@Column(name = "pick_up")
	private String pickUp;

	@Column(name = "drop_off")
	private String dropOff;
	
	private Date date;

	@ManyToOne(cascade = CascadeType.ALL)
	private Customer customer;

}
