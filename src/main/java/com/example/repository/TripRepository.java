package com.example.repository;

import com.example.model.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("tripRepository")
public interface TripRepository extends JpaRepository<Trip, Integer> {
}
