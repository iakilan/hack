package com.example.repository;

import com.example.model.Test;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository("testRepository")
public interface TestRepository extends JpaRepository<Test, Integer> {
}
