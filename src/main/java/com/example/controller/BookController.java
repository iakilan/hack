package com.example.controller;

import com.example.model.Book;
import com.example.model.Test;
import com.example.service.BookService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
@Api(value = "Book", description = "REST API for Book", tags = { "Book" })
public class BookController {

    @Autowired
    private BookService bookService;

    @ResponseStatus(code = HttpStatus.OK)
    @PostMapping(value = "/book", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    void testcreateTrip(@RequestBody Book book) {
       bookService.addBook(book);
    }

    //https://stackoverflow.com/questions/44032447/how-to-filter-attributes-from-json-response-in-spring
}
