package com.example.controller;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
@Api(value = "Login", description = "REST API login", tags = { "Login" })
public class AuthController {
    @PostMapping
    public void login() {

    }
}
