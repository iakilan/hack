package com.example.controller;

import com.example.model.Test;
import com.example.service.ITestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;

@Controller
@RequestMapping("/test")
@Api(value = "Test", description = "REST API for Test", tags = { "Test" })
public class TestController {

    @Autowired
    private ITestService testService;

    @ResponseStatus(code = HttpStatus.OK)
    @PostMapping(value = "/testtrip", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String testcreateTrip(@RequestBody Test test) {
        int id =  testService.addTestRepo(test);

        return "{\"testId\":\"" + Integer.toString(id) + "\"}";
    }

    @ApiOperation(value = "TEST BY ARUD")
    @GetMapping(value = "/test")
    public @ResponseBody Test getTest(@RequestParam int id) {
        // logger.info("Test method called");
        return this.testService.getTestRepo(id);
    }
}

