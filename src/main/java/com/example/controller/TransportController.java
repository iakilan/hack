package com.example.controller;

import com.example.model.Customer;
import com.example.model.Trip;
import com.example.service.ITestService;
import com.example.service.ITripService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.example.constant.AppConstant.HttpConstant.ROOT_URI;

/**
 * Rest controller for Transport Service
 */
@Controller
@RequestMapping("/")
@Api(value = "Trip", description = "REST API for Trip", tags = { "Trip" })
public class TransportController {
	// static Logger logger = Logger.getLogger(TransportController.class);

	@Autowired
	private ITripService tripService;

	@Autowired
	private ITestService testService;

	@DeleteMapping(value = ROOT_URI)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public @ResponseBody void removeTrip(@RequestParam int id) {
			tripService.removeTrip(id);
	}

	// TODO: need to add proper uri /trip/list
	// TODO: get the uri from app-constant
	@GetMapping(value = "/viewall")
	@ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody List<Trip> getAllTrip() {

		return this.tripService.listTrips();
	}

	// TODO: need to add proper uri /trip/{tripId}
	// TODO: get the uri from app-constant
	@GetMapping(value = "/viewtrip")
	@ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody Trip getTripById(@RequestParam int id) {

		return this.tripService.getTripById(id);
	}

	@ResponseStatus(code = HttpStatus.OK)
	@PostMapping(value = ROOT_URI, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String createTrip(@RequestBody @Valid Trip trip) {

		String createdTripId = Integer.toString(tripService.addTrip(trip));

		return "{\"tripId\":\"" + createdTripId + "\"}";
	}

	// TODO: need to add proper uri /trip/{customer}
	// TODO: get the uri from app-constant
	/**
	 * Delete all transport
	 * @param customer
	 * @return boolean result
	 */
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	@DeleteMapping(value="/transport/{customer}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody void deleteAllTransport(@RequestParam("customer") Customer customer) {
		this.tripService.removeAllTrip(customer);
	}

}
