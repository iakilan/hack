package com.example.service;

import com.example.model.Test;
import com.example.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("testService")
public class TestService implements ITestService {

	@Autowired
	private TestRepository testRepository;

	@Override
	@Transactional
	public int addTestRepo(Test test) {
		return testRepository.save(test).getId();
	}

	@Override
	@Transactional
	public Test getTestRepo(int id) {
		return testRepository.getOne(id);
	}

}
