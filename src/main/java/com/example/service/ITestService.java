package com.example.service;

import com.example.model.Test;

public interface ITestService {
	public int addTestRepo(Test test);
	public Test getTestRepo(int id);

}
