package com.example.service;

import com.example.model.Customer;

import java.util.List;

public interface ICustomerService {
	public Customer addCustomer(Customer customer);
	public List<Customer> listCustomer();
	public Customer getCustomerById(int id);
}
