package com.example.service;

import java.util.List;

import com.example.model.Customer;
import com.example.model.Trip;

public interface ITripService {

	public int addTrip(Trip trip);
	public List<Trip> listTrips();
	public Trip getTripById(int id);
	public void removeTrip(int id);
	public void removeAllTrip(Customer customer);
}
