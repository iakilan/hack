package com.example.service;

import com.example.model.Book;
import com.example.model.Publisher;
import com.example.repository.BookRepository;
import com.example.repository.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("bookService")
public class DefaultBookService implements BookService {

    @Autowired
    private BookRepository  bookRepository;

    @Autowired
    private PublisherRepository publisherRepository;

    @Override
    @Transactional
    public void addBook(Book book) {
        bookRepository.save(book);
    }
}
