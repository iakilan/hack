package com.example.service;

import com.example.model.Book;

public interface BookService {

    public void addBook(Book book);
}
