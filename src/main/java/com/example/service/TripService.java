package com.example.service;

import com.example.model.Customer;
import com.example.model.Trip;
import com.example.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service("tripService")
public class TripService implements ITripService {

	@Autowired
	private TripRepository tripRepository;

	@Override
	@Transactional
	public int addTrip(Trip trip) {
		trip.setDate( new Date() );

		return this.tripRepository.save( trip ).getTripId();
	}

	@Override
	@Transactional
	public List<Trip> listTrips() {

		return this.tripRepository.findAll();
	}

	@Override
	@Transactional
	public Trip getTripById(int id) {

		return this.tripRepository.getOne(id);
	}

	@Override
	@Transactional
	public void removeTrip(int id) {
		this.tripRepository.deleteById(id);
	}

	@Override
	@Transactional
	public void removeAllTrip(Customer customer) {
		this.tripRepository.deleteAll();
	}
}