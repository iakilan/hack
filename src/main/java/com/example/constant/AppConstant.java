package com.example.constant;

/**
 * Application constant holder
 */
public final class AppConstant {

    /**
     * Http constant holder
     */
    public static final class HttpConstant {
        public static final String ROOT_URI = "/trip";
    }
}
