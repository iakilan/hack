package com.example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.schema.Collections;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean

	public Docket productApi() {

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.example.controller")).paths(PathSelectors.any()).build();

	}
	//
	// https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api
	// https://dzone.com/articles/spring-boot-restful-api-documentation-with-swagger

//	private ApiInfo apiInfo() {
//		return new ApiInfo("My REST API", "Some custom description of API.", "API TOS", "Terms of service",
//				new Contact("John Doe", "www.example.com", "myeaddress@company.com"), "License of API",
//				"API license URL", Collections.emptyList());
//	}
}
